# Описание проекта

## Описание

Данный проект представляет собой реализацию классов для одной видеоигры. В игре есть Существа, к которым относятся Игрок
и Монстры. Классы существ описывают их параметры, возможности и методы для взаимодействия.

## Требования

Для корректной работы проекта необходимо установить и настроить следующие инструменты:

- Java 17
- Spring Boot 3.1.3
- Docker
- Docker Compose
- H2 Database
- PostgreSQL
- Spring Data JPA
- Spring Security
- Hibernate
- Swagger Open Api

## Установка и запуск

1. Склонируйте репозиторий на свой компьютер.

```
git clone https://gitlab.com/VladSemenovForVibeLab/game-in-the-form-of-rest-api.git
```

2. Установите все необходимые технологии и инструменты.

```
sudo apt-get update
```

```
sudo apt-get install openjdk-17-jdk
```

3. Откройте проект в выбранной IDE.

```
sudo snap install intellij-idea-ultimate --classic
```

4. Скачать Docker.

```
  sudo apt-get install docker-ce docker-ce-cli containerd.io
```

```
 curl -fsSL https://get.docker.com -o get-docker.sh
```

```
  sudo sh get-docker.sh
```

4. Запустите приложение с помощью Docker.

```
    docker compose up
```

5. Откройте `http://localhost:8080` веб-браузере для доступа к приложению.
6. По адресу `http://localhost:8080/swagger-ui/index.html` доступна документация

## Связь

Если у вас возникли вопросы или предложения по проекту, вы можете связаться со мной в Telegram:

- Пользователь: https://t.me/ProstoVladTut

Буду рад обратной связи и сотрудничеству!

Кроме того, в проекте предоставлены другие классы, такие как классы для работы с базой данных, классы для авторизации и
аутентификации пользователей, и другие классы, улучшающие функциональность игры.
