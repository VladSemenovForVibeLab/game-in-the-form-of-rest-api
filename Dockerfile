FROM gradle:8.3.0-jdk17 AS build
WORKDIR /
COPY /src /src
COPY build.gradle.kts .
COPY settings.gradle.kts .
RUN gradle clean build -x test

FROM openjdk:17-jdk-slim
WORKDIR /
COPY /src /src
COPY --from=build /build/libs/*.jar game.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "game.jar"]

LABEL authors="Vladislav-Semenov"
