package com.semenov.gameButNotAGame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameButNotAGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameButNotAGameApplication.class, args);
	}

}
