package com.semenov.gameButNotAGame.service.interf;

public interface CreatureService<E,K> {
    boolean attack(E entityAttackerId,K entityDefenderId);
}
