package com.semenov.gameButNotAGame.service.interf;

import com.semenov.gameButNotAGame.model.Monster;
import com.semenov.gameButNotAGame.model.Player;

public interface MonsterService extends CreatureService<Long, Long>,CRUDService<Monster,Long>{
}
