package com.semenov.gameButNotAGame.service.interf;

import com.semenov.gameButNotAGame.model.Monster;
import com.semenov.gameButNotAGame.model.Player;

public interface PlayerService extends CreatureService<Long, Long>,CRUDService<Player,Long> {
    void heal(Long entityId);

}
