package com.semenov.gameButNotAGame.service.interf;

import java.util.List;

public interface CRUDService<E,K> {
    E findById(K id);

    List<E> findAll();

    E update(E entity);

    void delete(E entity);

    void create(E entity);
}
