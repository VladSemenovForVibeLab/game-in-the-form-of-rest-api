package com.semenov.gameButNotAGame.service.interf;

import com.semenov.gameButNotAGame.model.user.User;

public interface UserService extends CRUDService<User,Long> {
    User getByUsername(String username);
}
