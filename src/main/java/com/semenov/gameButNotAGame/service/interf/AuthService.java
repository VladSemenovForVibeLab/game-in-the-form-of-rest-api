package com.semenov.gameButNotAGame.service.interf;

import com.semenov.gameButNotAGame.payload.request.JWTRequest;
import com.semenov.gameButNotAGame.payload.response.JWTResponse;

public interface AuthService {
    JWTResponse login(JWTRequest loginRequest);

    JWTResponse refresh(String refreshToken);
}

