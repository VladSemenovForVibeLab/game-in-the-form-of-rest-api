package com.semenov.gameButNotAGame.service.impl;

import com.semenov.gameButNotAGame.model.Monster;
import com.semenov.gameButNotAGame.model.Player;
import com.semenov.gameButNotAGame.repository.MonsterRepository;
import com.semenov.gameButNotAGame.repository.PlayerRepository;
import com.semenov.gameButNotAGame.service.interf.MonsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class MonsterServiceImpl extends AbstractCRUDService<Monster,Long> implements MonsterService {
    private final MonsterRepository monsterRepository;
    private final PlayerRepository playerRepository;

    @Override
    public boolean attack(Long entityAttackerId, Long entityDefenderId) {
        Optional<Player> optionalPlayer = playerRepository.findById(entityDefenderId);
        Optional<Monster> optionalMonster = monsterRepository.findById(entityAttackerId);
        boolean attackSuccess = false;
        if (optionalPlayer.isPresent() && optionalMonster.isPresent()) {
            Player player = optionalPlayer.get();
            Monster monster = optionalMonster.get();
            // Рассчитываем Модификатор атаки
            int attackModifier = monster.getAttack() - player.getDefense() + 1;
            attackModifier = Math.max(attackModifier, 1); // Минимальное значение 1

            // Генерируем бросок N кубиков
            int diceRolls = attackModifier;
            Random random = new Random();

            for (int i = 0; i < diceRolls; i++) {
                int diceResult = random.nextInt(6) + 1;
                if (diceResult == 5 || diceResult == 6) {
                    attackSuccess = true;
                    break; // Успешная атака, выходим из цикла
                }
            }

            if (attackSuccess) {
                // Удар успешен, выбираем произвольное значение из параметра Урон атакующего
                int damage = random.nextInt(monster.getMaxDamage() - monster.getMinDamage() + 1) + monster.getMinDamage();
                // Вычитаем урон из здоровья защищающегося
                player.setHealth(player.getHealth() - damage);
                // Проверяем, не умер ли защищающийся
                if (player.getHealth() <= 0) {
                    // Удаляем игрока из базы данных, так как он умер
                    playerRepository.deleteById(player.getId());
                } else {
                    // Обновляем здоровье игрока в базе данных
                    playerRepository.save(player);
                }
            }
        }
        return attackSuccess;
    }

    @Override
    JpaRepository<Monster, Long> getRepository() {
        return monsterRepository;
    }
}
