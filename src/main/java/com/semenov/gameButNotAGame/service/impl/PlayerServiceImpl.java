package com.semenov.gameButNotAGame.service.impl;

import com.semenov.gameButNotAGame.model.Monster;
import com.semenov.gameButNotAGame.model.Player;
import com.semenov.gameButNotAGame.repository.MonsterRepository;
import com.semenov.gameButNotAGame.repository.PlayerRepository;
import com.semenov.gameButNotAGame.service.interf.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class PlayerServiceImpl extends AbstractCRUDService<Player, Long> implements PlayerService {
    private final PlayerRepository playerRepository;
    private final MonsterRepository monsterRepository;

    @Override
    public boolean attack(Long entityAttackerId, Long entityDefenderId) {
        Optional<Player> optionalPlayer = playerRepository.findById(entityAttackerId);
        Optional<Monster> optionalMonster = monsterRepository.findById(entityDefenderId);
        boolean attackSuccess = false;
        if (optionalPlayer.isPresent() && optionalMonster.isPresent()) {
            Player player = optionalPlayer.get();
            Monster monster = optionalMonster.get();
            // Рассчитываем Модификатор атаки
            int attackModifier = player.getAttack() - monster.getDefense() + 1;
            attackModifier = Math.max(attackModifier, 1); // Минимальное значение 1

            // Генерируем бросок N кубиков
            int diceRolls = attackModifier;
            Random random = new Random();

            for (int i = 0; i < diceRolls; i++) {
                int diceResult = random.nextInt(6) + 1;
                if (diceResult == 5 || diceResult == 6) {
                    attackSuccess = true;
                    break; // Успешная атака, выходим из цикла
                }
            }

            if (attackSuccess) {
                // Удар успешен, выбираем произвольное значение из параметра Урон атакующего
                int damage = random.nextInt(player.getMaxDamage() - player.getMinDamage() + 1) + player.getMinDamage();
                // Вычитаем урон из здоровья защищающегося
                monster.setHealth(monster.getHealth() - damage);
                // Проверяем, не умер ли защищающийся
                if (monster.getHealth() <= 0) {
                    // Удаляем монстра из базы данных, так как он умер
                    monsterRepository.deleteById(monster.getId());
                } else {
                    // Обновляем здоровье монстра в базе данных
                    monsterRepository.save(monster);
                }
            }
        }
        return attackSuccess;
    }

    @Override
    public void heal(Long entityId) {
        Optional<Player> optionalPlayer = playerRepository.findById(entityId);
        if (optionalPlayer.isPresent()) {
            Player player = optionalPlayer.get();
            // Игрок может исцелиться до 4 раз на 30% от максимального Здоровья
            int maxHealAttempts = 4;
            int maxHealAmount = (int) (0.3 * player.getMaxHealth());

            for (int i = 0; i < maxHealAttempts; i++) {
                int healAmount = new Random().nextInt(maxHealAmount) + 1;
                player.setHealth(player.getHealth() + healAmount);
                // Ограничиваем здоровье игрока максимальным значением
                if (player.getHealth() > player.getMaxHealth()) {
                    player.setHealth(player.getMaxHealth());
                }
                // Обновляем здоровье игрока в базе данных
                playerRepository.save(player);
            }
        }
    }

    @Override
    JpaRepository<Player, Long> getRepository() {
        return playerRepository;
    }
}
