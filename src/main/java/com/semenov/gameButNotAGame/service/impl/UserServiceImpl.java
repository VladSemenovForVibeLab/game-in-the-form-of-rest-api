package com.semenov.gameButNotAGame.service.impl;

import com.semenov.gameButNotAGame.model.exception.ResourceNotFoundException;
import com.semenov.gameButNotAGame.model.user.Role;
import com.semenov.gameButNotAGame.model.user.User;
import com.semenov.gameButNotAGame.repository.UserRepository;
import com.semenov.gameButNotAGame.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl extends AbstractCRUDService<User,Long> implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Override
    JpaRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(()->new ResourceNotFoundException("User not found"));
    }
    @Override
    public User update(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userRepository.save(entity);
        return entity;
    }

    @Override
    @Transactional
    public void create(User entity) {
        if(userRepository.findByUsername(entity.getUsername()).isPresent()){
            throw new IllegalStateException("User already exists.");
        }
        if(!entity.getPassword().equals(entity.getPasswordConfirmation())){
            throw new IllegalStateException("Password and password confirmation do not match");
        }
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        Set<Role> roles = Set.of(Role.ROLE_USER);
        entity.setRoles(roles);
        userRepository.save(entity);
    }
}
