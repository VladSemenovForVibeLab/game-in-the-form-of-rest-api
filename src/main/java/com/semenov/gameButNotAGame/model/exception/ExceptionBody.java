package com.semenov.gameButNotAGame.model.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Map;

@NoArgsConstructor
@ToString
@Data
@AllArgsConstructor
public class ExceptionBody {
    private String message;
    private Map<String,String> errors;

    public ExceptionBody(String message) {
        this.message = message;
    }
}

