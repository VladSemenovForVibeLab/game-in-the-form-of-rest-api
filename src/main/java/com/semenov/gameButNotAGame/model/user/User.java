package com.semenov.gameButNotAGame.model.user;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.scheduling.config.Task;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "users")
@Schema(description = "Request for register")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Schema(description = "user_id",example = "1")
    private Long id;
    @Schema(description = "Username",example = "vlad@gmail.com")
    private String username;
    @Column(name = "name")
    @Schema(description = "Name",example = "Vladislav")
    private String name;
    @Schema(description = "Password",example = "123")
    private String password;
    @Transient
    @Schema(description = "Password confirmation",example = "123")
    private String passwordConfirmation;
    @Column(name = "role")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "users_roles")
    @Enumerated(value = EnumType.STRING)
    private Set<Role> roles;
}