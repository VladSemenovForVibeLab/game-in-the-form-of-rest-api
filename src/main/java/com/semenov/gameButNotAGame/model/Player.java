package com.semenov.gameButNotAGame.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "player")
public class Player extends Creature{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "attack")
    @Max(value = 30,message = "Attack cannot exceed 30")
    private int attack;
    @Column(name = "defense")
    @Max(value = 30, message = "Defense cannot exceed 30")
    private int defense;
    @Column(name = "health")
    private int health;
    @Column(name = "min_damage")
    private int minDamage;
    @Column(name = "max_damage")
    private int maxDamage;
    @Column(name = "name")
    private String name;
    @Column(name = "max_health")
    private Integer maxHealth;
    @Column(name = "level")
    private int level;
    @Column(name = "experience")
    private int experience;
}