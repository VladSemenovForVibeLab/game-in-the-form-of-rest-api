package com.semenov.gameButNotAGame.controller;

import com.semenov.gameButNotAGame.payload.response.MessageResponse;
import com.semenov.gameButNotAGame.service.interf.CRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CRUDRestController<E,K> {
    abstract CRUDService<E,K> getService();

    @PostMapping
    public ResponseEntity<MessageResponse> create(@RequestBody E entity){
        getService().create(entity);
        Map<E,Integer> createdMap = new HashMap<>();
        createdMap.put(entity,entity.hashCode());
        return new ResponseEntity<>(new MessageResponse("The entity was created",createdMap,getService().findAll()), HttpStatus.CREATED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<MessageResponse> findById(@PathVariable K id){
        E entity = getService().findById(id);
        if(entity==null){
            return new ResponseEntity<>(new MessageResponse("An entity cannot be found with this id"),HttpStatus.NOT_FOUND);
        }
        Map<E,Integer> entityFound = new HashMap<>();
        entityFound.put(entity,entity.hashCode());
        return new ResponseEntity<>(new MessageResponse("Entity with given id found",entityFound),HttpStatus.FOUND);
    }
    @GetMapping
    public ResponseEntity<MessageResponse> findAll(){
        List<E> objects = getService().findAll();
        return new ResponseEntity<>(new MessageResponse("All the entities are in front of you",objects),HttpStatus.OK);
    }
    @PutMapping
    public ResponseEntity<MessageResponse> update(@RequestBody E entity){
        E updatedObject = getService().update(entity);
        Map<E,Integer> createdMap = new HashMap<>();
        createdMap.put(updatedObject,updatedObject.hashCode());
        return new ResponseEntity<>(new MessageResponse("Here is a renewed essence",createdMap,getService().findAll()),HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<MessageResponse> delete(@PathVariable K id){
        E objectForDelete = getService().findById(id);
        getService().delete(objectForDelete);
        return new ResponseEntity<>(new MessageResponse("The entity has been deleted!"),HttpStatus.NO_CONTENT);
    }

}
