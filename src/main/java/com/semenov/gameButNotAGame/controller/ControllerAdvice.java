package com.semenov.gameButNotAGame.controller;

import com.semenov.gameButNotAGame.model.exception.ExceptionBody;
import io.jsonwebtoken.MalformedJwtException;
import jakarta.validation.ConstraintViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.security.SignatureException;
import java.util.Map;

@RestControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleIllegalState(IllegalStateException e) {
        return new ExceptionBody(e.getMessage());
    }
    @ExceptionHandler(TransactionSystemException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleTransactionSystem(TransactionSystemException e) {
        return new ExceptionBody("You were not validated, please try again",Map.of(e.getMessage(),e.getOriginalException().toString()));
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleHttpMessageNotReadable(HttpMessageNotReadableException e) {
        return new ExceptionBody("You have entered a value that cannot be parsed, please replace it",Map.of(e.getMessage(),e.getLocalizedMessage()));
    }
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleConstraintViolation(ConstraintViolationException e) {
        return new ExceptionBody("You were not validated, please try again",Map.of(e.getMessage(),e.getStackTrace().toString()));
    }
    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleInvalidDataAccessApiUsage(InvalidDataAccessApiUsageException e) {
        return new ExceptionBody("There are no entities with this id, it’s worth double-checking the data",Map.of(e.getMessage(),e.getStackTrace().toString()));
    }
    @ExceptionHandler(MalformedJwtException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleMalformedJwt(MalformedJwtException e) {
        return new ExceptionBody("JWT strings must contain exactly 2 period characters",Map.of(e.getMessage(),e.getStackTrace().toString()));
    }
    @ExceptionHandler(SignatureException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleSignature(SignatureException e) {
        return new ExceptionBody("Invalid token",Map.of(e.getMessage(),e.getStackTrace().toString()));
    }
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionBody handleException(Exception e) {
        return new ExceptionBody("Internal error");
    }
}
