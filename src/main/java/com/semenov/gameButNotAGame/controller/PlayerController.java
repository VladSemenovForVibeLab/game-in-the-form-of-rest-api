package com.semenov.gameButNotAGame.controller;

import com.semenov.gameButNotAGame.model.Monster;
import com.semenov.gameButNotAGame.model.Player;
import com.semenov.gameButNotAGame.payload.response.MessageResponse;
import com.semenov.gameButNotAGame.service.interf.CRUDService;
import com.semenov.gameButNotAGame.service.interf.MonsterService;
import com.semenov.gameButNotAGame.service.interf.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(PlayerController.PLAYER_REST_URL)
@RequiredArgsConstructor
public class PlayerController extends CRUDRestController<Player, Long> {
    public static final String PLAYER_REST_URL = "/api/v1/players";
    private final PlayerService playerService;
    private final MonsterService monsterService;

    @Override
    CRUDService<Player, Long> getService() {
        return playerService;
    }

    @PostMapping("/{playerId}/attack/{monsterId}")
    public ResponseEntity<MessageResponse> playerAttackMonster(@PathVariable Long playerId, @PathVariable Long monsterId) {
        boolean attack = playerService.attack(playerId, monsterId);
        if (attack) {
            Map<Player,Monster> attackMap = new HashMap<>();
            Player byIdPlayer = playerService.findById(playerId);
            Monster byIdMonster = monsterService.findById(monsterId);
            attackMap.put(byIdPlayer,byIdMonster);
            return new ResponseEntity<>(new MessageResponse("Attack successful",attackMap,playerService.findAll()),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new MessageResponse("Attack unsuccessful"),HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/{playerId}/heal")
    public ResponseEntity<MessageResponse> playerHeal(@PathVariable Long playerId) {
        playerService.heal(playerId);
        Map<Player,Integer> playerInformation = new HashMap<>();
        List<Player> players = getService().findAll();
        Player byId = playerService.findById(playerId);
        playerInformation.put(byId,byId.hashCode());
        return new ResponseEntity<>(new MessageResponse("The player healed himself to 30% of his maximum Health.",playerInformation,players),HttpStatus.OK);
    }
}
