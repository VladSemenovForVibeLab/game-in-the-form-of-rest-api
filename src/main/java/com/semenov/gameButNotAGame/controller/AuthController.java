package com.semenov.gameButNotAGame.controller;

import com.semenov.gameButNotAGame.model.user.User;
import com.semenov.gameButNotAGame.payload.request.JWTRequest;
import com.semenov.gameButNotAGame.payload.response.JWTResponse;
import com.semenov.gameButNotAGame.service.interf.AuthService;
import com.semenov.gameButNotAGame.service.interf.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
@Tag(name = "Authentication Controller",description = "Authentication API")
public class AuthController {
    private final AuthService authService;
    private final UserService userService;

    @PostMapping("/login")
    public JWTResponse login(@RequestBody JWTRequest loginRequest){
        return authService.login(loginRequest);
    }

    @PostMapping("/register")
    public void register(@RequestBody User userDto){
        userService.create(userDto);
    }

    @PostMapping("/refresh")
    public JWTResponse refresh(@RequestBody String refreshToken){
        return authService.refresh(refreshToken);
    }
}

