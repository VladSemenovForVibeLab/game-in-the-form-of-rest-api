package com.semenov.gameButNotAGame.controller;

import com.semenov.gameButNotAGame.model.Monster;
import com.semenov.gameButNotAGame.model.Player;
import com.semenov.gameButNotAGame.payload.response.MessageResponse;
import com.semenov.gameButNotAGame.service.interf.CRUDService;
import com.semenov.gameButNotAGame.service.interf.MonsterService;
import com.semenov.gameButNotAGame.service.interf.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(MonsterController.MONSTER_REST_URL)
@RequiredArgsConstructor
public class MonsterController extends CRUDRestController<Monster,Long> {
    public static final String MONSTER_REST_URL="/api/v1/monsters";
    private final MonsterService monsterService;
    private final PlayerService playerService;

    @Override
    CRUDService<Monster, Long> getService() {
        return monsterService;
    }
    @PostMapping("/{monsterId}/attack/{playerId}")
    public ResponseEntity<MessageResponse> monsterAttackPlayer(@PathVariable Long monsterId, @PathVariable Long playerId) {
        boolean attack = monsterService.attack(monsterId, playerId);
        if(attack){
            Map<Monster,Player> attackMap = new HashMap<>();
            Player byIdPlayer = playerService.findById(playerId);
            Monster byIdMonster = monsterService.findById(monsterId);
            attackMap.put(byIdMonster,byIdPlayer);
            return new ResponseEntity<>(new MessageResponse("Attack successful",attackMap,monsterService.findAll()), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new MessageResponse("Attack unsuccessful"),HttpStatus.BAD_REQUEST);
        }
    }
}
