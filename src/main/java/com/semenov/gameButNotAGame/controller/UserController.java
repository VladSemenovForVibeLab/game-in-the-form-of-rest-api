package com.semenov.gameButNotAGame.controller;

import com.semenov.gameButNotAGame.model.user.User;
import com.semenov.gameButNotAGame.service.interf.CRUDService;
import com.semenov.gameButNotAGame.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UserController.USER_REST_URL)
@RequiredArgsConstructor
public class UserController extends CRUDRestController<User,Long> {
    private final UserService userService;
    public static final String USER_REST_URL = "/api/v1/users";
    @Override
    CRUDService<User, Long> getService() {
        return userService;
    }
}
