package com.semenov.gameButNotAGame.payload.request;

import jakarta.persistence.Column;
import jakarta.validation.constraints.Max;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Builder
public class PayloadRequest {
    @Max(value = 30,message = "Attack cannot exceed 30")
    private int attack;
    @Max(value = 30, message = "Defense cannot exceed 30")
    private int defense;
    private int health;
    private int minDamage;
    private int maxDamage;
    private String name;
    private Integer maxHealth;
    private int level;
    private int experience;
}
