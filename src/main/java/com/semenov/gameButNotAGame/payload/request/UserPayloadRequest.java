package com.semenov.gameButNotAGame.payload.request;

import com.semenov.gameButNotAGame.model.user.Role;
import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class UserPayloadRequest {
    private String username;
    private String name;
    private String password;
    @Transient
    private String passwordConfirmation;
    @Column(name = "role")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "users_roles")
    @Enumerated(value = EnumType.STRING)
    private Set<Role> roles;
}
