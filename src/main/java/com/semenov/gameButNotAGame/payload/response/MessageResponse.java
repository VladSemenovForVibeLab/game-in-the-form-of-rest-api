package com.semenov.gameButNotAGame.payload.response;

import lombok.*;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class MessageResponse{
    private String message;
    private Map<?,?> entity;
    private List<?> entityList;
    public MessageResponse(String message) {
        this.message = message;
    }
    public MessageResponse(String message, Map<?, ?> entity) {
        this.message = message;
        this.entity = entity;
    }
    public MessageResponse(String message, List<?> entityList) {
        this.message = message;
        this.entityList = entityList;
    }
}
