-- Примеры данных для сущности Player
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (20, 15, 100, 10, 25, 'Player1', 100, 1, 0);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (25, 20, 120, 15, 30, 'Player2', 120, 2, 100);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (15, 10, 80, 8, 20, 'Player3', 80, 1, 0);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (22, 18, 110, 12, 28, 'Player4', 110, 2, 150);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (28, 22, 130, 18, 32, 'Player5', 130, 3, 300);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (18, 12, 90, 10, 24, 'Player6', 90, 1, 0);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (24, 19, 115, 14, 29, 'Player7', 115, 2, 200);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (30, 25, 140, 20, 35, 'Player8', 140, 3, 400);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (16, 11, 85, 9, 22, 'Player9', 85, 1, 0);
INSERT INTO player (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (23, 17, 105, 11, 27, 'Player10', 105, 2, 250);

-- Примеры данных для сущности Monster
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (15, 10, 80, 8, 20, 'Monster1', 80, 1, 0);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (18, 12, 90, 10, 24, 'Monster2', 90, 1, 0);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (22, 18, 110, 12, 28, 'Monster3', 110, 2, 150);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (25, 20, 120, 15, 30, 'Monster4', 120, 2, 200);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (30, 25, 140, 18, 35, 'Monster5', 140, 3, 400);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (17, 11, 85, 9, 22, 'Monster6', 85, 1, 0);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (21, 17, 105, 11, 27, 'Monster7', 105, 2, 250);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (27, 22, 130, 18, 32, 'Monster8', 130, 3, 300);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (19, 13, 95, 11, 25, 'Monster9', 95, 1, 0);
INSERT INTO monster (attack, defense, health, min_damage, max_damage, name, max_health, level, experience) VALUES (24, 19, 115, 14, 29, 'Monster10', 115, 2, 200);